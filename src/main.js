import './main.css';
import 'primevue/resources/themes/bootstrap4-dark-blue/theme.css';
import {createApp} from 'vue';
import PrimeVue from 'primevue/config';
import App from './App.vue';

createApp(App)
    .use(PrimeVue)
    .mount('#app');
