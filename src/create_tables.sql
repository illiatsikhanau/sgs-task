-- Creating the table for cities
CREATE TABLE cities (
    id INT PRIMARY KEY,
    name NVARCHAR(255) NOT NULL
);

-- Creating the table for workshops
CREATE TABLE workshops (
    id INT PRIMARY KEY,
    name NVARCHAR(255) NOT NULL,
    city_id INT,
    FOREIGN KEY (city_id) REFERENCES cities(id)
);

-- Creating the table for employees
CREATE TABLE employees (
    id INT PRIMARY KEY,
    name NVARCHAR(255) NOT NULL,
    workshop_id INT,
    FOREIGN KEY (workshop_id) REFERENCES workshops(id)
);

-- Creating the table for shifts
CREATE TABLE shifts (
    id INT PRIMARY KEY,
    name NVARCHAR(255) NOT NULL
);
